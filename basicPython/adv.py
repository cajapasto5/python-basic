

import random

def run():
    n_r = random.randint(1,100)
    n_e = int(input("Elige número: "))

    while n_e != n_r:
        if(n_e<n_r):
            print("El número es más grande, elige de nuevo.")
        else:
            print("El número es más pequeño, elige de nuevo.")
        n_e = int(input("Elige número: "))
    print("Ganaste")

if __name__ == '__main__':
    run()