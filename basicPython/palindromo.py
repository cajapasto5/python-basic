""" data = input("Ingresa texto: ")

palindromo= data[::-1]

if(data==palindromo):
    print("palindromo")
else:
    print("no es palindromo") """


def palindromo(palabra):
    palabra = palabra.replace(' ','')
    palabra = palabra.lower()
    palabra_invertida = palabra[::-1]
    if(palabra==palabra_invertida):
        return True
    else:
        return False


def run():
    palabra = input("Escribe palabra: ")
    es_palindromo = palindromo(palabra) 
    if es_palindromo:
        print("Es palindromo")
    else:
        print("No es palindromo")


#punto de entrada un programa de python
if __name__ == '__main__':
    run()
