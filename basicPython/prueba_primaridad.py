def es_primo(n):
    if n == 1:
        return False
    else:
        count = 0
    for i in range(1,n+1):
        if i == 1 or i == n:
            continue
        if n % i ==0:
            count +=1
    
    if count == 0:
        return True
    else: 
        return False


def run():
    n = int(input("Escribe número: "))
    if es_primo(n):
        print("Es primo")
    else:
        print("No es primo")

if __name__ == '__main__':
    run()